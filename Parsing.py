TV2 = "1 0 132 1 216 1"
x = 132 + (255 * 1)
group = [132, 1]
x2 = group[0] + (255 * group[-1])

touchVals = "4 0 10 0 94 0 31 0 94 0 52 0 94 0 73 0 94 0"
positionsList = [
    (10, 94),
    (31, 94),
    (52, 94),
    (94, 73)
]
# TV2 = "1 0 132 1 216 1"
def floorPositions(data):
    numVals = data.split()
    xYList = []
    groupList = []
    for i in range(int(data[0]) * 2):
        startSlice = (i * 2) + 2
        group = numVals[startSlice:startSlice+2]
        pixelLocation = int(group[0]) + (int(group[1]) * 255)
        groupList.append(pixelLocation)
        if (i + 1) % 2 == 0:
            xYList.append(groupList)
            groupList = []
    # print(groupList)
    return xYList

for pos in floorPositions(touchVals):
    print(pos)
